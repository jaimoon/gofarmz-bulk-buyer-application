package com.example.jai.gofarmzbulkbuyerapplication.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.jai.gofarmzbulkbuyerapplication.R;
import com.example.jai.gofarmzbulkbuyerapplication.adapters.AddressAdapter;
import com.example.jai.gofarmzbulkbuyerapplication.models.AddressModel;
import com.example.jai.gofarmzbulkbuyerapplication.utils.NetworkChecking;

import java.util.ArrayList;

public class ChooseAddressActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView close_img;
    ArrayList<AddressModel> arrayList=new ArrayList<>();
    RecyclerView address_recyclerview;
    AddressAdapter addressAdapter;
    RecyclerView.LayoutManager layoutManager;
    Button add_new_address_btn;
    private boolean checkInternet;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_address);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        close_img=findViewById(R.id.close_img);
        close_img.setOnClickListener(this);

        add_new_address_btn=findViewById(R.id.add_new_address_btn);
        add_new_address_btn.setOnClickListener(this);

        address_recyclerview=findViewById(R.id.address_recyclerview);

        arrayList.add(new AddressModel("ganesh","addressline1","addressline2","madhapur","hyderabad","telangana","500043","8801200610"));
        arrayList.add(new AddressModel("ganesh","addressline1","addressline2","madhapur","hyderabad","telangana","500043","8801200610"));
        arrayList.add(new AddressModel("ganesh","addressline1","addressline2","madhapur","hyderabad","telangana","500043","8801200610"));
        arrayList.add(new AddressModel("ganesh","addressline1","addressline2","madhapur","hyderabad","telangana","500043","8801200610"));
        arrayList.add(new AddressModel("ganesh","addressline1","addressline2","madhapur","hyderabad","telangana","500043","8801200610"));
        addressAdapter=new AddressAdapter(arrayList,ChooseAddressActivity.this);
        layoutManager=new LinearLayoutManager(ChooseAddressActivity.this);
        address_recyclerview.setNestedScrollingEnabled(false);
        address_recyclerview.setLayoutManager(layoutManager);
        address_recyclerview.setAdapter(addressAdapter);
    }

    @Override
    public void onClick(View v) {
        if (v == close_img){
            finish();
        }
        if (v == add_new_address_btn){
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet)
            {
                Intent intent = new Intent(ChooseAddressActivity.this, AddAddressActivity.class);
                intent.putExtra("activity","ChooseAddress");
                startActivity(intent);

            }
            else
            {
                Toast.makeText(this, "Please Check Your Intenet Connection", Toast.LENGTH_SHORT).show();
            }
        }

    }
}
