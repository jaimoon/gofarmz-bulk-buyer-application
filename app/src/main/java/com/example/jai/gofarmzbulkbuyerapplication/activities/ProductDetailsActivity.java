package com.example.jai.gofarmzbulkbuyerapplication.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.example.jai.gofarmzbulkbuyerapplication.R;


public class ProductDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView close_img,cart_img;
    Button btnViewCart;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.productdetails_activity);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        close_img=findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        cart_img=findViewById(R.id.cart_img);
        cart_img.setOnClickListener(this);
        btnViewCart=findViewById(R.id.btnViewCart);
        btnViewCart.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v == close_img){
            finish();
        }
        if (v == cart_img){
            startActivity(new Intent(ProductDetailsActivity.this,CartActivity.class));
        }
        if (v == btnViewCart){
            startActivity(new Intent(ProductDetailsActivity.this,CartActivity.class));
        }

    }
}
