package com.example.jai.gofarmzbulkbuyerapplication.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jai.gofarmzbulkbuyerapplication.R;
import com.example.jai.gofarmzbulkbuyerapplication.activities.PaymentDetailsActivity;
import com.example.jai.gofarmzbulkbuyerapplication.models.CheckoutProductDetailsResponse;
import com.example.jai.gofarmzbulkbuyerapplication.models.PaymentProductModel;

import java.util.ArrayList;
import java.util.List;

public class PaymentProductDetailsAdapter extends RecyclerView.Adapter<PaymentProductDetailsAdapter.Holder> {
    List<CheckoutProductDetailsResponse> arrayList;
    Context context;
    public PaymentProductDetailsAdapter(List<CheckoutProductDetailsResponse> arrayList, PaymentDetailsActivity paymentDetailsActivity) {
        this.arrayList=arrayList;
        this.context=paymentDetailsActivity;
    }

    @NonNull
    @Override
    public PaymentProductDetailsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_product_payment_detail, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentProductDetailsAdapter.Holder holder, int i) {

        holder.product_name.setText(arrayList.get(i).pdtName);
        holder.qty_btn.setText(arrayList.get(i).unitValue);
        holder.price_txt.setText(arrayList.get(i).price);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();

    }

    class Holder extends RecyclerView.ViewHolder{
        TextView product_name,qty_btn,price_txt;

        public Holder(@NonNull View itemView) {
            super(itemView);

            price_txt=itemView.findViewById(R.id.price_txt);
            qty_btn=itemView.findViewById(R.id.qty_btn);
            product_name=itemView.findViewById(R.id.product_name);
        }
    }

}
