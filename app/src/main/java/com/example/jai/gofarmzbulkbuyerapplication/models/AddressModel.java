package com.example.jai.gofarmzbulkbuyerapplication.models;

public class AddressModel {
    String name,address1,address2,area,city,state,pincode,mobileno;

    public AddressModel(String name, String address1, String address2, String area, String city, String state, String pincode, String mobileno) {
        this.name = name;
        this.address1 = address1;
        this.address2 = address2;
        this.area = area;
        this.city = city;
        this.state = state;
        this.pincode = pincode;
        this.mobileno = mobileno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }
}
