package com.example.jai.gofarmzbulkbuyerapplication.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.jai.gofarmzbulkbuyerapplication.MainActivity;
import com.example.jai.gofarmzbulkbuyerapplication.R;
import com.example.jai.gofarmzbulkbuyerapplication.models.ChangePasswordresponse;
import com.example.jai.gofarmzbulkbuyerapplication.utils.ApiClient;
import com.example.jai.gofarmzbulkbuyerapplication.utils.ApiInterface;
import com.example.jai.gofarmzbulkbuyerapplication.utils.NetworkChecking;
import com.example.jai.gofarmzbulkbuyerapplication.utils.UserSessionManager;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {
    EditText old_password_edt,new_password_edt,conf_password_edt;
    Button submit_btn;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String deviceId,user_id,token,old_pass,new_pass,confrm_pass;
    ApiInterface apiInterface;
    ImageView close_img;
    ProgressDialog pprogressDialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.changepassword_activity);


        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA",deviceId+"/n"+user_id+"/n"+token);

        old_password_edt=findViewById(R.id.old_password_edt);
        new_password_edt=findViewById(R.id.new_password_edt);
        conf_password_edt=findViewById(R.id.conf_password_edt);
        close_img=findViewById(R.id.close_img);
        close_img.setOnClickListener(this);

        submit_btn=findViewById(R.id.submit_btn);
        submit_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == close_img){
            finish();
        }
        if (v == submit_btn) {

            if (validate()) {
                checkInternet = NetworkChecking.isConnected(this);
                if (checkInternet) {

            pprogressDialog = new ProgressDialog(ChangePasswordActivity.this);
            pprogressDialog.setMessage("Please wait......");
            pprogressDialog.setProgressStyle(R.style.DialogTheme);
            pprogressDialog.show();
                    apiInterface= ApiClient.getClient().create(ApiInterface.class);
                    String url=ApiClient.BASE_URL+"api/user/change-password";
                    Call<ChangePasswordresponse> changePasswordresponseCall=apiInterface.ChangePassword("ANDROID",deviceId,token,url,user_id,old_password_edt.getText().toString(),new_password_edt.getText().toString());
                    changePasswordresponseCall.enqueue(new Callback<ChangePasswordresponse>() {
                        @Override
                        public void onResponse(Call<ChangePasswordresponse> call, Response<ChangePasswordresponse> response) {
                            if (response.isSuccessful());
                            ChangePasswordresponse changePasswordresponse=response.body();
                            if (changePasswordresponse.getStatus().equals("10100")){
                                pprogressDialog.dismiss();
                                Toast.makeText(ChangePasswordActivity.this, changePasswordresponse.getMessage(), Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(ChangePasswordActivity.this, MainActivity.class);
                                startActivity(intent);
                            }
                            if (changePasswordresponse.getStatus().equals("10200")) {
                                pprogressDialog.dismiss();
                                Toast.makeText(getApplicationContext(),  changePasswordresponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                            if (changePasswordresponse.getStatus().equals("10300")) {
                                pprogressDialog.dismiss();
                                Toast.makeText(getApplicationContext(),  changePasswordresponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                            if (changePasswordresponse.getStatus().equals("10400")) {
                                pprogressDialog.dismiss();
                                Toast.makeText(getApplicationContext(),  changePasswordresponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                            if (changePasswordresponse.getStatus().equals("10500")) {
                                pprogressDialog.dismiss();
                                Toast.makeText(getApplicationContext(),  changePasswordresponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                            if (changePasswordresponse.getStatus().equals("10600")) {
                                pprogressDialog.dismiss();
                                Toast.makeText(getApplicationContext(),  changePasswordresponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ChangePasswordresponse> call, Throwable t) {
                            pprogressDialog.dismiss();
                            Toast.makeText(ChangePasswordActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });


                }
            }
            else {
                Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
            }


        }
    }

    private boolean validate() {

        boolean result = true;
        int flag = 0;

        String strCurrentPassword = old_password_edt.getText().toString();

        if (strCurrentPassword == null || strCurrentPassword.equals("") ||strCurrentPassword.length()==0)
        {
            old_password_edt.setError("Please enter old password");
            result = false;
        }


        String strNewPassword = new_password_edt.getText().toString();

        if (strNewPassword == null || strNewPassword.equals("") || strNewPassword.matches("^-\\s")||strNewPassword.length()==0)
        {
            new_password_edt.setError("Please enter new password");
            result = false;

        }

        String strConformPassword = conf_password_edt.getText().toString();

        if (strConformPassword == null || strConformPassword.equals("")|| strConformPassword.matches("^-\\s") ||strConformPassword.length()==0)
        {
            conf_password_edt.setError("Please enter confirm password");
            result = false;

        }

     /*  if(strNewPassword != "" && strConformPassword != "" &&!strConformPassword.equals(strNewPassword) && flag == 0 )
        {
            new_password_edt.setError("New Password and Confirm password mismatch");
            conf_password_edt.setError("New Password and Confirm password mismatch");
            result = false;
        }
       else {

           new_password_edt.setError("New Password and Confirm password mismatch");
           conf_password_edt.setError("New Password and Confirm password mismatch");
       }*/

        return result;

    }
}
