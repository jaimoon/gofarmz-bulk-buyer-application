package com.example.jai.gofarmzbulkbuyerapplication.models;

import com.google.gson.annotations.SerializedName;

public class GetOTPModel {

    /**
     * status : 10100
     * message : OTP has been sent to your mobile mumber.
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
