package com.example.jai.gofarmzbulkbuyerapplication.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaos.view.PinView;
import com.example.jai.gofarmzbulkbuyerapplication.R;
import com.example.jai.gofarmzbulkbuyerapplication.models.ForgotOtpResponse;
import com.example.jai.gofarmzbulkbuyerapplication.models.ForgotOtpVerifyResponse;
import com.example.jai.gofarmzbulkbuyerapplication.utils.ApiClient;
import com.example.jai.gofarmzbulkbuyerapplication.utils.ApiInterface;
import com.example.jai.gofarmzbulkbuyerapplication.utils.NetworkChecking;
import com.example.jai.gofarmzbulkbuyerapplication.utils.UserSessionManager;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener{
    TextView otp_txt, txtresend;
    PinView pinView;
    EditText password_edt;
    Button verify_btn;
    private boolean checkInternet;
    String mobile, deviceId, send_mobile,otp;
    UserSessionManager userSessionManager;

    Dialog dialog;
    private Boolean exit = false;
    ApiInterface apiInterface;
    ProgressDialog pprogressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);

        password_edt = findViewById(R.id.password_edt);
        otp_txt = findViewById(R.id.otp_txt);
//        otp_txt.setTypeface(bold);
        txtresend = findViewById(R.id.txtresend);
        txtresend.setPaintFlags(txtresend.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
//        txtresend.setTypeface(light);
        txtresend.setOnClickListener(this);

        pinView = findViewById(R.id.pinView);
//        pinView.setTypeface(light);

        verify_btn = findViewById(R.id.verify_btn);
//        verify_btn.setTypeface(light);
        verify_btn.setOnClickListener(this);


        getAlertBox();


    }

    private void getAlertBox() {

        dialog = new Dialog(ForgotPasswordActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog_mobile);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_layout_dialog);
        TextView dialog_title = dialog.findViewById(R.id.dialog_title);
        final EditText edt_send_mobile = dialog.findViewById(R.id.edt_send_mobile);
        final TextView sendmobileText =  dialog.findViewById(R.id.sendmobileText);
        final ImageView close_dialog =  dialog.findViewById(R.id.close_dialog);

        dialog.setCanceledOnTouchOutside(false);
        sendmobileText.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {


                pprogressDialog = new ProgressDialog(ForgotPasswordActivity.this);
                pprogressDialog.setMessage("Please wait......");
                pprogressDialog.setProgressStyle(R.style.DialogTheme);
                pprogressDialog.show();

                Log.d("eererere","tdgssdfertdf");
                sendmobileText.setClickable(false);
                sendmobileText.setFocusableInTouchMode(false);
                send_mobile=edt_send_mobile.getText().toString();

                if(send_mobile == null || send_mobile.equals("") || send_mobile.length() < 0)
                {
                    edt_send_mobile.setError("Please Enter Mobile No...!");
                }
                else
                {
                    dialog.cancel();
                    Log.d("eererere",send_mobile);
                    getOTP(send_mobile);


                }
            }
        });

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.dismiss();
                finish();
            }
        });


        dialog.show();

    }

    private void getOTP(String send_mobile) {

        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            apiInterface= ApiClient.getClient().create(ApiInterface.class);

            Call<ForgotOtpResponse> call=apiInterface.ForgotOtp(send_mobile);
            call.enqueue(new Callback<ForgotOtpResponse>() {
                @Override
                public void onResponse(Call<ForgotOtpResponse> call, Response<ForgotOtpResponse> response) {
                    if (response.isSuccessful());
                    ForgotOtpResponse forgotOtpResponse=response.body();
                    if (forgotOtpResponse.status.equals("10100")){
                        pprogressDialog.dismiss();
                        Toast.makeText(ForgotPasswordActivity.this, forgotOtpResponse.message, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                    else if (forgotOtpResponse.status.equals("10300")){
                        pprogressDialog.dismiss();
                        Toast.makeText(ForgotPasswordActivity.this, forgotOtpResponse.message, Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ForgotOtpResponse> call, Throwable t) {
                    pprogressDialog.dismiss();
                    Toast.makeText(ForgotPasswordActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    dialog.dismiss();

                }
            });

        }
        else {
            Toast.makeText(ForgotPasswordActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == verify_btn) {
            verifyOtp();
        }

    }


    private boolean validate() {
        boolean result = true;
        otp = pinView.getText().toString().trim();
        if ((otp == null || otp.equals("")) || otp.length() != 6) {
            pinView.setError("Invalid OTP");
            result = false;
        }
        return result;
    }
    private void verifyOtp() {

        checkInternet = NetworkChecking.isConnected(this);
        if (validate()) {

            if (checkInternet) {
                pprogressDialog = new ProgressDialog(ForgotPasswordActivity.this);
                pprogressDialog.setMessage("Please wait......");
                pprogressDialog.setProgressStyle(R.style.DialogTheme);
                pprogressDialog.show();
                apiInterface= ApiClient.getClient().create(ApiInterface.class);
                Call<ForgotOtpVerifyResponse> call=apiInterface.ForgotOtpVerify(password_edt.getText().toString(),otp,"111",send_mobile);
                call.enqueue(new Callback<ForgotOtpVerifyResponse>() {
                    @Override
                    public void onResponse(Call<ForgotOtpVerifyResponse> call, Response<ForgotOtpVerifyResponse> response) {
                        if (response.isSuccessful());
                        ForgotOtpVerifyResponse forgotOtpVerifyResponse=response.body();
                        if (forgotOtpVerifyResponse.status.equals("10100")){
                            pprogressDialog.dismiss();
                            Toast.makeText(ForgotPasswordActivity.this, forgotOtpVerifyResponse.message, Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(ForgotPasswordActivity.this,LoginActivity.class));
                            finish();

                        }else if (forgotOtpVerifyResponse.status.equals("10300")){
                            pprogressDialog.dismiss();
                            Toast.makeText(ForgotPasswordActivity.this, forgotOtpVerifyResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ForgotOtpVerifyResponse> call, Throwable t) {
                        pprogressDialog.dismiss();
                        Toast.makeText(ForgotPasswordActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

            }
            else {
                Toast.makeText(ForgotPasswordActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }

    }

}
