package com.example.jai.gofarmzbulkbuyerapplication.models;

public class CartModel {
    int image;
    String p_name,p_price;

    public CartModel(int image, String p_name, String p_price) {
        this.image = image;
        this.p_name = p_name;
        this.p_price = p_price;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public String getP_price() {
        return p_price;
    }

    public void setP_price(String p_price) {
        this.p_price = p_price;
    }
}
