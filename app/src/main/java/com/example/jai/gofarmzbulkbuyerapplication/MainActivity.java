package com.example.jai.gofarmzbulkbuyerapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jai.gofarmzbulkbuyerapplication.activities.CartActivity;
import com.example.jai.gofarmzbulkbuyerapplication.activities.MyOrdersActivity;
import com.example.jai.gofarmzbulkbuyerapplication.activities.NotificationsActivity;
import com.example.jai.gofarmzbulkbuyerapplication.activities.ProfileActivity;
import com.example.jai.gofarmzbulkbuyerapplication.adapters.CartRecyclerAdapter;
import com.example.jai.gofarmzbulkbuyerapplication.adapters.CategoryAdapter;
import com.example.jai.gofarmzbulkbuyerapplication.models.CartModel;
import com.example.jai.gofarmzbulkbuyerapplication.models.CategoryModel;
import com.example.jai.gofarmzbulkbuyerapplication.utils.ApiClient;
import com.example.jai.gofarmzbulkbuyerapplication.utils.ApiInterface;
import com.example.jai.gofarmzbulkbuyerapplication.utils.NetworkChecking;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView cat_recyclerview;
    CategoryAdapter categoryAdapter;
    ApiInterface apiInterface;
    GridLayoutManager gridLayoutManager;
    TextView cart_count_txt, price_txt, box_name_txt, kyf_txt, story_txt, fn_txt, support_txt, terms_faq_policies_txt, profile_txt, orders_txt, share_txt, notifications_txt, ref_farmer_menu, preorders_txt,notification_menu;
    ArrayList<CartModel> arrayList=new ArrayList<>();
    CategoryAdapter cartRecyclerAdapter;
    RecyclerView.LayoutManager layoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        cat_recyclerview = findViewById(R.id.cat_recyclerview);
        profile_txt=findViewById(R.id.profile_txt);
        profile_txt.setOnClickListener(this);
        orders_txt=findViewById(R.id.orders_txt);
        orders_txt.setOnClickListener(this);
        share_txt=findViewById(R.id.share_txt);
        share_txt.setOnClickListener(this);
        notifications_txt=findViewById(R.id.notifications_txt);
        notifications_txt.setOnClickListener(this);
        getCategories();
    }

    private void getCategories() {
        arrayList.add(new CartModel(R.drawable.vegetable_img,"Categories","500"));
        arrayList.add(new CartModel(R.drawable.vegetable_img,"Categories","500"));
        arrayList.add(new CartModel(R.drawable.vegetable_img,"Categories","500"));
        arrayList.add(new CartModel(R.drawable.vegetable_img,"Categories","500"));
        arrayList.add(new CartModel(R.drawable.vegetable_img,"Categories","500"));
        arrayList.add(new CartModel(R.drawable.vegetable_img,"Categories","500"));
        arrayList.add(new CartModel(R.drawable.vegetable_img,"Categories","500"));
        cartRecyclerAdapter=new CategoryAdapter(arrayList, MainActivity.this);
        layoutManager=new GridLayoutManager(MainActivity.this,2);
        cat_recyclerview.setNestedScrollingEnabled(false);
        cat_recyclerview.setLayoutManager(layoutManager);
        cat_recyclerview.setAdapter(cartRecyclerAdapter);

//        apiInterface = ApiClient.getClient().create(ApiInterface.class);
//        final Call<CategoryModel> categoryModelCall = apiInterface.categoryModel();
//        categoryModelCall.enqueue(new Callback<CategoryModel>() {
//            @Override
//            public void onResponse(Call<CategoryModel> call, Response<CategoryModel> response) {
//                if (response.isSuccessful()) ;
//
//                CategoryModel categoryModel = response.body();
//                final List<CategoryModel.DataBean> categoryBeanList = response.body() != null ? response.body().getData() : null;
//
//                if (categoryModel.getStatus().equalsIgnoreCase("10100")) {
//
//                    categoryAdapter = new CategoryAdapter(categoryBeanList, MainActivity.this);
//                    gridLayoutManager = new GridLayoutManager(MainActivity.this, 2);
//                    cat_recyclerview.setLayoutManager(gridLayoutManager);
//                    cat_recyclerview.setItemAnimator(new DefaultItemAnimator());
//                    cat_recyclerview.setHasFixedSize(true);
//                    cat_recyclerview.setAdapter(categoryAdapter);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CategoryModel> call, Throwable t) {
//
//            }
//        });
    }

    @Override
    public void onClick(View v) {
        if (v == profile_txt) {
            Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
            startActivity(intent);
        }
        if (v == orders_txt) {
            Intent intent = new Intent(MainActivity.this, MyOrdersActivity.class);
            intent.putExtra("R_id","orders");
            startActivity(intent);
        }
        if (v == share_txt) {
            boolean checkInternet = NetworkChecking.isConnected(MainActivity.this);
            if (checkInternet) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "GoFarmz -");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Hi, Here is an excellent app which supplies genuine naturally grown food products, sourced from local farmers, straight to your doorstep. Download to Check it out.  " + "https://play.google.com/store/apps/details?id=in.innasoft.gofarmz&hl=en");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            } else {
                Toast.makeText(this, "No Internet connection...!", Toast.LENGTH_SHORT).show();
            }
        }
        if (v == notifications_txt) {
            Intent intent = new Intent(MainActivity.this, NotificationsActivity.class);
            startActivity(intent);
//            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        }
    }
}
