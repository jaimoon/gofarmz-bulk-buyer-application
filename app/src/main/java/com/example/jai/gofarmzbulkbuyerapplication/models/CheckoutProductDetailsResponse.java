package com.example.jai.gofarmzbulkbuyerapplication.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class CheckoutProductDetailsResponse {

    @SerializedName("pdtId")
    @Expose
    public String pdtId;
    @SerializedName("optId")
    @Expose
    public String optId;
    @SerializedName("pdtName")
    @Expose
    public String pdtName;
    @SerializedName("images")
    @Expose
    public String images;
    @SerializedName("unitValue")
    @Expose
    public String unitValue;
    @SerializedName("unitName")
    @Expose
    public String unitName;
    @SerializedName("price")
    @Expose
    public String price;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("pdtId", pdtId).append("optId", optId).append("pdtName", pdtName).append("images", images).append("unitValue", unitValue).append("unitName", unitName).append("price", price).toString();
    }
}
