package com.example.jai.gofarmzbulkbuyerapplication.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class CategoryModelData {


    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("type_avail")
    @Expose
    public String typeAvail;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("image", image).append("typeAvail", typeAvail).toString();
    }
}
