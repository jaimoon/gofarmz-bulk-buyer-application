package com.example.jai.gofarmzbulkbuyerapplication.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.jai.gofarmzbulkbuyerapplication.MyCartItemClickListener;
import com.example.jai.gofarmzbulkbuyerapplication.R;
import com.example.jai.gofarmzbulkbuyerapplication.activities.PaymentDetailsActivity;
import com.example.jai.gofarmzbulkbuyerapplication.models.CheckoutPaymentGatewayResponse;
import com.example.jai.gofarmzbulkbuyerapplication.utils.NetworkChecking;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Map;

public class PaymentTypeAdapter extends RecyclerView.Adapter<PaymentTypeAdapter.Holder> {
    List<CheckoutPaymentGatewayResponse> checkoutPaymentGatewayResponse;
    Context context;
    private RadioButton lastCheckedRB = null;
    public String send_check_Value, pay_id;
    private boolean checkInternet;
    public PaymentTypeAdapter(List<CheckoutPaymentGatewayResponse> checkoutPaymentGatewayResponse, PaymentDetailsActivity paymentDetailsActivity) {
        this.checkoutPaymentGatewayResponse=checkoutPaymentGatewayResponse;
        this.context=paymentDetailsActivity;
    }

    @NonNull
    @Override
    public PaymentTypeAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_pay_type, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PaymentTypeAdapter.Holder holder, final int i) {

        holder.pay_radio_dynamic_button.setText(checkoutPaymentGatewayResponse.get(i).name);

        Picasso.with(context).load("http://gofarmzv2.learningslot.in/"+checkoutPaymentGatewayResponse.get(i).logo).placeholder(R.drawable.cart).into(holder.pay_logo);


        if (i == 0) {
            if (lastCheckedRB == null) {
                pay_id = checkoutPaymentGatewayResponse.get(i).id;
                holder.pay_radio_dynamic_button.setChecked(true);
                lastCheckedRB = holder.pay_radio_dynamic_button;
                Log.d("eeee", "" + pay_id);
            }
        }

        View.OnClickListener rbClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {
                    RadioButton checked_rb = (RadioButton) v;
                    if (lastCheckedRB != null) {

                        lastCheckedRB.setChecked(false);
                        pay_id = checkoutPaymentGatewayResponse.get(i).id;
                        Log.d("zzzzzz", "" + pay_id);
                        send_check_Value = holder.pay_radio_dynamic_button.getText().toString();
                    }
                    lastCheckedRB = checked_rb;
                    pay_id = checkoutPaymentGatewayResponse.get(i).id;
                    send_check_Value = holder.pay_radio_dynamic_button.getText().toString();
                }else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        };

        holder.pay_radio_dynamic_button.setOnClickListener(rbClick);
        holder.pay_radio_dynamic_button.setTag(i);

        holder.setItemClickListener(new MyCartItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return checkoutPaymentGatewayResponse.size();
    }

    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener{
        RadioButton pay_radio_dynamic_button;
        ImageView pay_logo;
        MyCartItemClickListener citiesItemClickListener;

        public Holder(@NonNull View itemView) {
            super(itemView);
            pay_logo=itemView.findViewById(R.id.pay_logo);
            pay_radio_dynamic_button=itemView.findViewById(R.id.pay_radio_dynamic_button);
        }

        @Override
        public void onClick(View v) {
            this.citiesItemClickListener.onItemClick(v, getLayoutPosition());
        }

        public void setItemClickListener(MyCartItemClickListener ic) {
            this.citiesItemClickListener = ic;
        }
    }
}
