package com.example.jai.gofarmzbulkbuyerapplication.models;

public class CategoryDetailsModel {

    int image;
    String name;
    String quantitiy;

    public CategoryDetailsModel(int image, String name, String quantitiy) {
        this.image = image;
        this.name = name;
        this.quantitiy = quantitiy;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantitiy() {
        return quantitiy;
    }

    public void setQuantitiy(String quantitiy) {
        this.quantitiy = quantitiy;
    }
}
