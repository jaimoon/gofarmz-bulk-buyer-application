package com.example.jai.gofarmzbulkbuyerapplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jai.gofarmzbulkbuyerapplication.R;
import com.example.jai.gofarmzbulkbuyerapplication.activities.ProductDetailsActivity;
import com.example.jai.gofarmzbulkbuyerapplication.models.CategoryDetailsModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Map;
public class CategoryProductAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<CategoryDetailsModel> boxProducts = new ArrayList<>();

    public CategoryProductAdapter(Context ctx, ArrayList<CategoryDetailsModel> tmp) {
        context = ctx;
        boxProducts = tmp;
    }

    @Override
    public int getCount() {
        return boxProducts.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.row_category_products, null, true);

        ImageView product_img = view.findViewById(R.id.product_img);
        Picasso.with(context).load(boxProducts.get(position).getImage()).into(product_img);

        TextView product_name_txt = view.findViewById(R.id.product_name_txt);
        product_name_txt.setText(boxProducts.get(position).getName());


        TextView qty_txt = view.findViewById(R.id.qty_txt);
        qty_txt.setText(boxProducts.get(position).getQuantitiy());
        //+ (String) tmp.get("unitName"));

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                context.startActivity(new Intent(context, ProductDetailsActivity.class));
            }
        });



        return view;
    }
}
