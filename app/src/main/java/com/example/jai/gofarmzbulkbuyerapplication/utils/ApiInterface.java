package com.example.jai.gofarmzbulkbuyerapplication.utils;

import com.example.jai.gofarmzbulkbuyerapplication.models.CategoryModel;
import com.example.jai.gofarmzbulkbuyerapplication.models.ChangePasswordresponse;
import com.example.jai.gofarmzbulkbuyerapplication.models.CheckoutResponse;
import com.example.jai.gofarmzbulkbuyerapplication.models.EditProfileResponse;
import com.example.jai.gofarmzbulkbuyerapplication.models.ForgotOtpResponse;
import com.example.jai.gofarmzbulkbuyerapplication.models.ForgotOtpVerifyResponse;
import com.example.jai.gofarmzbulkbuyerapplication.models.GetOTPModel;
import com.example.jai.gofarmzbulkbuyerapplication.models.LoginModel;
import com.example.jai.gofarmzbulkbuyerapplication.models.LoginResponse;
import com.example.jai.gofarmzbulkbuyerapplication.models.LogoutResponse;
import com.example.jai.gofarmzbulkbuyerapplication.models.ProfileResponse;
import com.example.jai.gofarmzbulkbuyerapplication.models.RegisterResponse;
import com.example.jai.gofarmzbulkbuyerapplication.models.ResendOtpResponse;
import com.example.jai.gofarmzbulkbuyerapplication.models.VerifyOtpResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("user_api/login_app")
    Call<LoginResponse> loginModel(@Field("email") String email,
                                   @Field("password") String password,
                                   @Field("browser_id") String browser_id);
    @FormUrlEncoded
    @POST("user_api/registration")
    Call<RegisterResponse> Signup(@Field("name") String name,
                                @Field("email") String email,
                                @Field("mobile") String mobile,
                                @Field("conf_pwd") String conf_pwd);


    @FormUrlEncoded
    @POST("user_api/otp_verification")
    Call<VerifyOtpResponse> verifyOTPModel(@Field("otp") String otp,
                                           @Field("mobile") String mobile,
                                           @Field("browser_id") String browser_id);

    @FormUrlEncoded
    @POST("user_api/otp_resend")
    Call<ResendOtpResponse> getOTPModel(@Field("mobile") String mobile);


    @FormUrlEncoded
    @POST("user_api/forgot_password")
    Call<ForgotOtpResponse> ForgotOtp(@Field("mobile") String mobile);

    @FormUrlEncoded
    @POST("user_api/otp_verification_forgot_password")
    Call<ForgotOtpVerifyResponse> ForgotOtpVerify(@Field("new_pwd") String new_pwd,
                                                  @Field("otp") String otp,
                                                  @Field("browser_id") String browser_id,
                                                  @Field("mobile") String mobile);

    @FormUrlEncoded
    @POST("user_api/getUserProfile")
    Call<ProfileResponse> Profile(@Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("user_api/updateUserProfile")
    Call<EditProfileResponse> EditProfile(@Field("user_id") String user_id,
                                          @Field("name") String name,
                                          @Field("email") String email,
                                          @Field("mobile") String mobile,
                                          @Field("gender") String gender);


    @FormUrlEncoded
    @POST()
    Call<ChangePasswordresponse> ChangePassword(@Header("X-Platform") String platform,
                                                @Header("X-Deviceid") String device_id,
                                                @Header("Authorization-Basic") String token,
                                                @Url String url,
                                                @Field("user_id") String user_id,
                                                @Field("old_pwd") String old_pwd,
                                                @Field("new_pwd") String new_pwd);


    @GET()
    Call<CheckoutResponse> CheckoutDetails(@Header("X-Platform") String platform,
                                           @Header("X-Deviceid") String device_id,
                                           @Header("Authorization-Basic") String token,
                                           @Url String url);

}
