package com.example.jai.gofarmzbulkbuyerapplication.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class ProfileAddressResponse {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("address_line1")
    @Expose
    public String addressLine1;
    @SerializedName("address_line2")
    @Expose
    public String addressLine2;
    @SerializedName("area")
    @Expose
    public String area;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("state")
    @Expose
    public String state;
    @SerializedName("country")
    @Expose
    public String country;
    @SerializedName("pincode")
    @Expose
    public String pincode;
    @SerializedName("latitude")
    @Expose
    public String latitude;
    @SerializedName("longitude")
    @Expose
    public String longitude;
    @SerializedName("contact_no")
    @Expose
    public String contactNo;
    @SerializedName("alternate_contact_no")
    @Expose
    public String alternateContactNo;
    @SerializedName("is_default")
    @Expose
    public String isDefault;
    @SerializedName("created_on")
    @Expose
    public String createdOn;
    @SerializedName("updated_on")
    @Expose
    public String updatedOn;
    @SerializedName("distance")
    @Expose
    public Integer distance;
    @SerializedName("delivery_status")
    @Expose
    public Boolean deliveryStatus;
    @SerializedName("delivery_charges")
    @Expose
    public String deliveryCharges;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("userId", userId).append("name", name).append("addressLine1", addressLine1).append("addressLine2", addressLine2).append("area", area).append("city", city).append("state", state).append("country", country).append("pincode", pincode).append("latitude", latitude).append("longitude", longitude).append("contactNo", contactNo).append("alternateContactNo", alternateContactNo).append("isDefault", isDefault).append("createdOn", createdOn).append("updatedOn", updatedOn).append("distance", distance).append("deliveryStatus", deliveryStatus).append("deliveryCharges", deliveryCharges).toString();
    }

}
