package com.example.jai.gofarmzbulkbuyerapplication;

import android.view.View;

public interface MyCartItemClickListener
{
    void onItemClick(View v, int pos);
}

