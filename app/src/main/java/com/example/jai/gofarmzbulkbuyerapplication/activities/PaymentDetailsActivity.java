package com.example.jai.gofarmzbulkbuyerapplication.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jai.gofarmzbulkbuyerapplication.R;
import com.example.jai.gofarmzbulkbuyerapplication.adapters.CartRecyclerAdapter;
import com.example.jai.gofarmzbulkbuyerapplication.adapters.PaymentProductDetailsAdapter;
import com.example.jai.gofarmzbulkbuyerapplication.adapters.PaymentTypeAdapter;
import com.example.jai.gofarmzbulkbuyerapplication.models.CartModel;
import com.example.jai.gofarmzbulkbuyerapplication.models.CheckoutAddressResponse;
import com.example.jai.gofarmzbulkbuyerapplication.models.CheckoutDataResponse;
import com.example.jai.gofarmzbulkbuyerapplication.models.CheckoutPaymentGatewayResponse;
import com.example.jai.gofarmzbulkbuyerapplication.models.CheckoutProductDetailsResponse;
import com.example.jai.gofarmzbulkbuyerapplication.models.CheckoutProductResponse;
import com.example.jai.gofarmzbulkbuyerapplication.models.CheckoutResponse;
import com.example.jai.gofarmzbulkbuyerapplication.models.PaymentProductModel;
import com.example.jai.gofarmzbulkbuyerapplication.utils.ApiClient;
import com.example.jai.gofarmzbulkbuyerapplication.utils.ApiInterface;
import com.example.jai.gofarmzbulkbuyerapplication.utils.NetworkChecking;
import com.example.jai.gofarmzbulkbuyerapplication.utils.UserSessionManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView close_img;
    Button next_btn;
    SweetAlertDialog sweetAlertDialog;
    RecyclerView product_details_recyclerview,payment_type_recyclerview;
    ArrayList<PaymentProductModel> arrayList=new ArrayList<>();
    PaymentProductDetailsAdapter paymentProductDetailsAdapter;
    PaymentTypeAdapter paymentTypeAdapter;
    RecyclerView.LayoutManager layoutManager;
    ApiInterface apiInterface;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    String deviceId, token, user_id;
    ProgressDialog pprogressDialog;
    TableRow row_coupons;
    TextView manage_address_txt, addAddress_txt, delivery_title_txt, delivery_detail_txt, product_title_txt, payment_title_txt, add_coupan_code, price_title, pricetxt, coupan_code_txt, discount_title,
            discounttxt, delivery_title, dileverytxt, deliverytext_nocharge, totlaPrice_title, totlaPricetxt, payment_type_txt, promo_code_txt, allsponsors_toolbar_title, expected_delevry_title, expected_delevry_date;
    String delivery_charges, adress_id, appliedCoupon_amount;
    Float d_charges;
    Button total_btn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_activity);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        close_img=findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        next_btn=findViewById(R.id.next_btn);
        next_btn.setOnClickListener(this);
        product_details_recyclerview=findViewById(R.id.product_details_recyclerview);
        payment_type_recyclerview=findViewById(R.id.payment_type_recyclerview);
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();

        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
//        pprogressDialog = new ProgressDialog(PaymentDetailsActivity.this);
//        pprogressDialog.setMessage("Please wait......");
//        pprogressDialog.setProgressStyle(R.style.DialogTheme);
//        pprogressDialog.show();

        row_coupons=findViewById(R.id.row_coupons);
        pricetxt=findViewById(R.id.pricetxt);
        total_btn=findViewById(R.id.total_btn);
        dileverytxt=findViewById(R.id.dileverytxt);
        totlaPricetxt=findViewById(R.id.totlaPricetxt);
        getCheckoutData();
//        arrayList.add(new PaymentProductModel("tamoto","qty 2","500/-"));
//        arrayList.add(new PaymentProductModel("tamoto","qty 2","500/-"));
//        arrayList.add(new PaymentProductModel("tamoto","qty 2","500/-"));
//        arrayList.add(new PaymentProductModel("tamoto","qty 2","500/-"));
//        arrayList.add(new PaymentProductModel("tamoto","qty 2","500/-"));
//        arrayList.add(new PaymentProductModel("tamoto","qty 2","500/-"));
//
//        paymentProductDetailsAdapter=new PaymentProductDetailsAdapter(arrayList,PaymentDetailsActivity.this);
//        layoutManager=new LinearLayoutManager(PaymentDetailsActivity.this);
//        product_details_recyclerview.setNestedScrollingEnabled(false);
//        product_details_recyclerview.setLayoutManager(layoutManager);
//        product_details_recyclerview.setAdapter(paymentProductDetailsAdapter);

    }

    private void getCheckoutData() {

        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {

            apiInterface= ApiClient.getClient().create(ApiInterface.class);
//            String url=ApiClient.BASE_URL+"api/checkout?user_id=";
//            Call<CheckoutResponse> checkoutResponseCall=apiInterface.CheckoutDetails("ANDROID",deviceId,token,url+user_id);
//            checkoutResponseCall.enqueue(new Callback<CheckoutResponse>() {
//                @Override
//                public void onResponse(Call<CheckoutResponse> call, Response<CheckoutResponse> response) {
//                    if (response.isSuccessful());
//                    CheckoutResponse checkoutResponse=response.body();
//                    if (checkoutResponse.status.equals("10100")){
//                        pprogressDialog.dismiss();
//                        CheckoutDataResponse checkoutDataResponse=checkoutResponse.data;
//                        List<CheckoutPaymentGatewayResponse> checkoutPaymentGatewayResponse=checkoutDataResponse.paymentGateway;
//                        List<CheckoutProductResponse> checkoutProductResponse=checkoutDataResponse.products;
//                        List<CheckoutAddressResponse> checkoutAddressResponses=checkoutDataResponse.address;
//                        for (int i=0;i<checkoutProductResponse.size();i++) {
//                            pricetxt.setText(""+checkoutProductResponse.get(i).totalPrice);
//                            List<CheckoutProductDetailsResponse> checkoutProductDetailsResponse = checkoutProductResponse.get(i).eSSENTIAL;
//                            paymentProductDetailsAdapter=new PaymentProductDetailsAdapter(checkoutProductDetailsResponse,PaymentDetailsActivity.this);
//                            layoutManager=new LinearLayoutManager(PaymentDetailsActivity.this);
//                            product_details_recyclerview.setNestedScrollingEnabled(false);
//                            product_details_recyclerview.setLayoutManager(layoutManager);
//                            product_details_recyclerview.setItemAnimator(new DefaultItemAnimator());
//                            product_details_recyclerview.setAdapter(paymentProductDetailsAdapter);
//
//                            paymentTypeAdapter=new PaymentTypeAdapter(checkoutPaymentGatewayResponse,PaymentDetailsActivity.this);
//                            layoutManager=new LinearLayoutManager(PaymentDetailsActivity.this);
//                            payment_type_recyclerview.setNestedScrollingEnabled(false);
//                            payment_type_recyclerview.setLayoutManager(layoutManager);
//                            payment_type_recyclerview.setItemAnimator(new DefaultItemAnimator());
//                            payment_type_recyclerview.setAdapter(paymentTypeAdapter);
//
//                            row_coupons.setVisibility(View.VISIBLE);
//                            for (int j=0;j<checkoutAddressResponses.size();j++){
//                                d_charges = Float.parseFloat(checkoutAddressResponses.get(j).deliveryCharges);
//                                dileverytxt.setText("" + d_charges);
//                            }
//
//
//                            double result = Math.round(d_charges + Float.parseFloat(String.valueOf(checkoutDataResponse.finalTotal)));
//                            String priceResult = String.format("%.2f", result);
//                            totlaPricetxt.setText(priceResult);
//                            total_btn.setText("Total: " + priceResult);
//                        }
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<CheckoutResponse> call, Throwable t) {
//                    pprogressDialog.dismiss();
//                    Toast.makeText(PaymentDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
//
//                }
//            });

        }
    }

    @Override
    public void onClick(View v) {
        if (v == close_img){


            finish();
        }
        if (v == next_btn){

            sweetAlertDialog = new SweetAlertDialog(PaymentDetailsActivity.this, SweetAlertDialog.SUCCESS_TYPE);
            sweetAlertDialog.setTitleText("Thank You!!");
            sweetAlertDialog.setContentText("Order has been confirmed.!");
            sweetAlertDialog.setConfirmText("OK");
            sweetAlertDialog.showCancelButton(true);
            sweetAlertDialog.setCanceledOnTouchOutside(false);
            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(final SweetAlertDialog sweetAlertDialog) {
                    Intent order_detail = new Intent(PaymentDetailsActivity.this, MyOrdersActivity.class);
                    startActivity(order_detail);
                    finish();
                }

            });
            sweetAlertDialog.show();
        }

    }
}
