package com.example.jai.gofarmzbulkbuyerapplication.models;

import com.google.gson.annotations.SerializedName;

public class LoginModel {

    /**
     * status : 10100
     * message : You have been logged in successfully.
     * data : {"jwt":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1NTU5Mjk0NDEsImp0aSI6IlBpNzNWTXh6dHhjZTF4K2F0d3JLNTR2ZE93bXJaVFV1WFJ3WDZMZWR4MUU9IiwiaXNzIjoiaHR0cDpcL1wvZ29mYXJtenYyLmxlYXJuaW5nc2xvdC5pblwvcGhwLWpzb25cLyIsIm5iZiI6MTU1NTkyOTQ0MiwiZXhwIjoxNTg3NDY1NDQyLCJkYXRhIjp7InVzZXJfaWQiOiI1OCIsInVzZXJfbmFtZSI6IkpheSIsImVtYWlsIjoiamF5YWNoYW5kcmFAaW5uYXNvZnQuaW4iLCJtb2JpbGUiOiI5OTQ5OTA1NTQwIiwiYWNjb3VudF9zdGF0dXMiOiIxIiwiYnJvd3Nlcl9zZXNzaW9uX2lkIjoiUGk3M1ZNeHp0eGNlMXgrYXR3cks1NHZkT3dtclpUVXVYUndYNkxlZHgxRT0ifX0.1x9KWtO68TONXZs8TvMvMaCiYlX-GYxZ_5GQqEm3LRwHyZFuvvL3Z_UEMkew28dUDfvzmafiV_YZ5chrUU76MA","user_id":"58","user_name":"Jay","email":"jayachandra@innasoft.in","mobile":"9949905540"}
     */

    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * jwt : eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1NTU5Mjk0NDEsImp0aSI6IlBpNzNWTXh6dHhjZTF4K2F0d3JLNTR2ZE93bXJaVFV1WFJ3WDZMZWR4MUU9IiwiaXNzIjoiaHR0cDpcL1wvZ29mYXJtenYyLmxlYXJuaW5nc2xvdC5pblwvcGhwLWpzb25cLyIsIm5iZiI6MTU1NTkyOTQ0MiwiZXhwIjoxNTg3NDY1NDQyLCJkYXRhIjp7InVzZXJfaWQiOiI1OCIsInVzZXJfbmFtZSI6IkpheSIsImVtYWlsIjoiamF5YWNoYW5kcmFAaW5uYXNvZnQuaW4iLCJtb2JpbGUiOiI5OTQ5OTA1NTQwIiwiYWNjb3VudF9zdGF0dXMiOiIxIiwiYnJvd3Nlcl9zZXNzaW9uX2lkIjoiUGk3M1ZNeHp0eGNlMXgrYXR3cks1NHZkT3dtclpUVXVYUndYNkxlZHgxRT0ifX0.1x9KWtO68TONXZs8TvMvMaCiYlX-GYxZ_5GQqEm3LRwHyZFuvvL3Z_UEMkew28dUDfvzmafiV_YZ5chrUU76MA
         * user_id : 58
         * user_name : Jay
         * email : jayachandra@innasoft.in
         * mobile : 9949905540
         */

        @SerializedName("jwt")
        private String jwt;
        @SerializedName("user_id")
        private String userId;
        @SerializedName("user_name")
        private String userName;
        @SerializedName("email")
        private String email;
        @SerializedName("mobile")
        private String mobile;

        public String getJwt() {
            return jwt;
        }

        public void setJwt(String jwt) {
            this.jwt = jwt;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }
    }
}
