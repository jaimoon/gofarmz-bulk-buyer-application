package com.example.jai.gofarmzbulkbuyerapplication.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jai.gofarmzbulkbuyerapplication.R;
import com.example.jai.gofarmzbulkbuyerapplication.models.RegisterResponse;
import com.example.jai.gofarmzbulkbuyerapplication.utils.ApiClient;
import com.example.jai.gofarmzbulkbuyerapplication.utils.ApiInterface;
import com.example.jai.gofarmzbulkbuyerapplication.utils.NetworkChecking;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class    RegistrationActivity extends AppCompatActivity {

    @BindView(R.id.name_edt)
    EditText nameEdt;
    @BindView(R.id.email_edt)
    EditText emailEdt;
    @BindView(R.id.mobile_edt)
    EditText mobileEdt;
    @BindView(R.id.regpassword_edt)
    EditText regpasswordEdt;
    @BindView(R.id.t_c_chk)
    CheckBox tCChk;
    @BindView(R.id.t_c_textview)
    TextView tCTextview;
    @BindView(R.id.term_conditon_layout)
    RelativeLayout termConditonLayout;
    @BindView(R.id.reg_next_txt)
    Button regNextTxt;
    @BindView(R.id.or)
    TextView or;
    @BindView(R.id.signup_txt)
    Button signupTxt;
    @BindView(R.id.reg_rl)
    RelativeLayout regRl;
    private boolean checkInternet;
    String name,email,password,mobile;
    String MOBILE_REGEX = "^[6789]\\d{9}$";
    ApiInterface apiInterface;
    ProgressDialog pprogressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        checkInternet = NetworkChecking.isConnected(this);
    }

    @OnClick({R.id.reg_next_txt, R.id.signup_txt})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.reg_next_txt:
                reg();
                break;
            case R.id.signup_txt:
                login();
                break;
        }
    }

    private void login() {
        if (checkInternet) {
            Intent intent = new Intent(RegistrationActivity.this, LoginActivity.class);
            startActivity(intent);
        }else {
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    private void reg() {
        if (checkInternet) {
            name = nameEdt.getText().toString();
            email = emailEdt.getText().toString();
            password = regpasswordEdt.getText().toString();
            mobile = mobileEdt.getText().toString();
            if(name.length()==0){
                Toast.makeText(RegistrationActivity.this,"Please Enter Name",Toast.LENGTH_SHORT).show();
            }else if(!isEmailValid(email) || email.length()==0) {
                Toast.makeText(RegistrationActivity.this,"Please Enter Valid Email",Toast.LENGTH_SHORT).show();
            }else if(mobile.length()==0 || !mobile.matches(MOBILE_REGEX)){
                Toast.makeText(RegistrationActivity.this,"Please enter Valid Moblie Number",Toast.LENGTH_SHORT).show();
            }else if(password.length()==0){
                Toast.makeText(RegistrationActivity.this,"Please Enter Password",Toast.LENGTH_SHORT).show();
            }else if(!tCChk.isChecked())
            {
                Toast.makeText(getApplicationContext(), "Please Check Terms & Conditions", Toast.LENGTH_SHORT).show();
            }
            else
            {
                pprogressDialog = new ProgressDialog(RegistrationActivity.this);
                pprogressDialog.setMessage("Please wait......");
                pprogressDialog.setProgressStyle(R.style.DialogTheme);
                pprogressDialog.show();

                apiInterface=ApiClient.getClient().create(ApiInterface.class);
                Call<RegisterResponse> call=apiInterface.Signup(name,email,mobile,password);
                call.enqueue(new Callback<RegisterResponse>() {
                    @Override
                    public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                        if (response.isSuccessful());
                        RegisterResponse registerResponse=response.body();
                        if (registerResponse.status.equals("10100")){
                            pprogressDialog.dismiss();
                            Toast.makeText(RegistrationActivity.this, registerResponse.message, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(RegistrationActivity.this, AccountVerificationActivity.class);
                            intent.putExtra("mobile", mobile);
                            intent.putExtra("activity","Reg");
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onFailure(Call<RegisterResponse> call, Throwable t) {
                        pprogressDialog.dismiss();
                        Toast.makeText(RegistrationActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        } else {
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
}
