package com.example.jai.gofarmzbulkbuyerapplication.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.example.jai.gofarmzbulkbuyerapplication.R;
import com.example.jai.gofarmzbulkbuyerapplication.adapters.CartRecyclerAdapter;
import com.example.jai.gofarmzbulkbuyerapplication.models.CartModel;

import java.util.ArrayList;

public class CartActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView close_img;
    RecyclerView cart_recyclerview;
    ArrayList<CartModel> arrayList=new ArrayList<>();
    CartRecyclerAdapter cartRecyclerAdapter;
    RecyclerView.LayoutManager layoutManager;
    Button proceed_btn;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cart_activity);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        proceed_btn=findViewById(R.id.proceed_btn);
        proceed_btn.setOnClickListener(this);
        cart_recyclerview=findViewById(R.id.cart_recyclerview);

        arrayList.add(new CartModel(R.drawable.vegetable_img,"Tamoto","500"));
        arrayList.add(new CartModel(R.drawable.vegetable_img,"Tamoto","500"));
        arrayList.add(new CartModel(R.drawable.vegetable_img,"Tamoto","500"));
        arrayList.add(new CartModel(R.drawable.vegetable_img,"Tamoto","500"));
        arrayList.add(new CartModel(R.drawable.vegetable_img,"Tamoto","500"));
        arrayList.add(new CartModel(R.drawable.vegetable_img,"Tamoto","500"));
        arrayList.add(new CartModel(R.drawable.vegetable_img,"Tamoto","500"));
        cartRecyclerAdapter=new CartRecyclerAdapter(arrayList,CartActivity.this);


        layoutManager=new LinearLayoutManager(CartActivity.this);
        cart_recyclerview.setNestedScrollingEnabled(false);
        cart_recyclerview.setLayoutManager(layoutManager);
        cart_recyclerview.setAdapter(cartRecyclerAdapter);


        close_img=findViewById(R.id.close_img);


        close_img.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == close_img){
            finish();
        }
        if (v == proceed_btn){
            startActivity(new Intent(CartActivity.this,DeliverySlotActivity.class));
        }

    }
}
