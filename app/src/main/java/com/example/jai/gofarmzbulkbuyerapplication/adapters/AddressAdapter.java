package com.example.jai.gofarmzbulkbuyerapplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jai.gofarmzbulkbuyerapplication.R;
import com.example.jai.gofarmzbulkbuyerapplication.activities.ChooseAddressActivity;
import com.example.jai.gofarmzbulkbuyerapplication.activities.EditAddressActivity;
import com.example.jai.gofarmzbulkbuyerapplication.models.AddressModel;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.Holder> {
    ArrayList<AddressModel> arrayList;
    Context context;
    SweetAlertDialog sweetAlertDialog;
    public AddressAdapter(ArrayList<AddressModel> arrayList, ChooseAddressActivity chooseAddressActivity) {
        this.arrayList=arrayList;
        this.context=chooseAddressActivity;
    }

    @NonNull
    @Override
    public AddressAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_address, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AddressAdapter.Holder holder, final int i) {

        holder.name_txt.setText(arrayList.get(i).getName());
        holder.address_one_txt.setText(arrayList.get(i).getAddress1());
        holder.address_two_txt.setText(arrayList.get(i).getAddress2());
        holder.area_txt.setText(arrayList.get(i).getCity());
        holder.state_txt.setText(arrayList.get(i).getState());
        holder.pincode_txt.setText(arrayList.get(i).getPincode());
        holder.mobile_txt.setText(arrayList.get(i).getMobileno());

        holder.delete_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
                sweetAlertDialog.setTitleText("Are you sure?");
                sweetAlertDialog.setContentText("This product will be delete from cart!");
                sweetAlertDialog.setCancelText("No,cancel pls!");
                sweetAlertDialog.setConfirmText("Yes,delete it!");
                sweetAlertDialog.showCancelButton(true);

                sweetAlertDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                });
                sweetAlertDialog.show();



            }
        });

        holder.edit_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent intent=new Intent(context, EditAddressActivity.class);

               intent.putExtra("id","value");
               context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class Holder extends RecyclerView.ViewHolder{
        TextView edit_text,delete_text,name_txt,address_one_txt, address_two_txt,  area_txt,city_txt,state_txt,
                pincode_txt,mobile_txt,selectAddText;

        public Holder(@NonNull View itemView) {
            super(itemView);
            selectAddText=itemView.findViewById(R.id.selectAddText);

            edit_text=itemView.findViewById(R.id.edit_text);
            delete_text=itemView.findViewById(R.id.delete_text);

            name_txt=itemView.findViewById(R.id.name_txt);
            address_one_txt=itemView.findViewById(R.id.address_one_txt);
            address_two_txt=itemView.findViewById(R.id.address_two_txt);
            area_txt=itemView.findViewById(R.id.area_txt);
            city_txt=itemView.findViewById(R.id.city_txt);
            state_txt=itemView.findViewById(R.id.state_txt);
            pincode_txt=itemView.findViewById(R.id.pincode_txt);
            mobile_txt=itemView.findViewById(R.id.mobile_txt);

        }
    }
}
