package com.example.jai.gofarmzbulkbuyerapplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jai.gofarmzbulkbuyerapplication.MainActivity;
import com.example.jai.gofarmzbulkbuyerapplication.R;
import com.example.jai.gofarmzbulkbuyerapplication.activities.CategoryDetailsActivity;
import com.example.jai.gofarmzbulkbuyerapplication.models.CartModel;
import com.example.jai.gofarmzbulkbuyerapplication.models.CategoryModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;

import static com.example.jai.gofarmzbulkbuyerapplication.utils.ApiClient.IMAGE_URL;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.Holder> {

    private MainActivity context;
    private LayoutInflater li;
    private int resource;
    private Typeface light,regular,bold;
    private boolean checkInternet;
    ArrayList<CartModel> categoryBeanList;

    String id,type;

    public CategoryAdapter(ArrayList<CartModel> categoryBeanList, MainActivity mainActivity) {
        this.categoryBeanList=categoryBeanList;
        this.context=mainActivity;
    }


    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_category, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryAdapter.Holder holder, int i) {

        holder.category_title.setText(categoryBeanList.get(i).getP_name());

        Picasso.with(context)
                .load("http://gofarmzv2.learningslot.in/"+categoryBeanList.get(i).getImage())
                .placeholder(R.drawable.ic_launcher_background)
                .into(holder.img_categroy);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, CategoryDetailsActivity.class));
            }
        });

    }

    @Override
    public int getItemCount() {

        return  categoryBeanList.size();
    }

    class Holder extends RecyclerView.ViewHolder{
        TextView category_title;
        ImageView img_categroy;

        public Holder(@NonNull View itemView) {
            super(itemView);

            img_categroy=itemView.findViewById(R.id.img_categroy);
            category_title=itemView.findViewById(R.id.category_title);
        }
    }
}
