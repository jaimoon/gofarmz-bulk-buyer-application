package com.example.jai.gofarmzbulkbuyerapplication.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaos.view.PinView;
import com.example.jai.gofarmzbulkbuyerapplication.MainActivity;
import com.example.jai.gofarmzbulkbuyerapplication.R;
import com.example.jai.gofarmzbulkbuyerapplication.models.GetOTPModel;
import com.example.jai.gofarmzbulkbuyerapplication.models.ResendOtpResponse;
import com.example.jai.gofarmzbulkbuyerapplication.models.VerifyOtpDataResponse;
import com.example.jai.gofarmzbulkbuyerapplication.models.VerifyOtpResponse;
import com.example.jai.gofarmzbulkbuyerapplication.utils.ApiClient;
import com.example.jai.gofarmzbulkbuyerapplication.utils.ApiInterface;
import com.example.jai.gofarmzbulkbuyerapplication.utils.NetworkChecking;
import com.example.jai.gofarmzbulkbuyerapplication.utils.UserSessionManager;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountVerificationActivity extends AppCompatActivity {

    @BindView(R.id.logo_img)
    ImageView logoImg;
    @BindView(R.id.otp_txt)
    TextView otpTxt;
    @BindView(R.id.pinView)
    PinView pinView;
    @BindView(R.id.verify_btn)
    Button verifyBtn;
    @BindView(R.id.txtresend)
    TextView txtresend;
    UserSessionManager userSessionManager;
    String deviceId,mobile,activity,jwt,email,user_id,user_name;
    private boolean checkInternet;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_verification);
        ButterKnife.bind(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        checkInternet = NetworkChecking.isConnected(this);

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        mobile = bundle.getString("mobile");
        activity = bundle.getString("activity");

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
    }

    @OnClick({R.id.verify_btn, R.id.txtresend})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.verify_btn:
                verify();
                break;
            case R.id.txtresend:
                getOTP();
                break;
        }
    }

    private boolean validate() {
        boolean result = true;
        String otp = pinView.getText().toString().trim();
        if ((otp == null || otp.equals("")) || otp.length() != 6) {
            pinView.setError("Invalid OTP");
            result = false;
        }
        return result;
    }

    private void verify() {
        if (validate()) {
            if (checkInternet) {

                final ProgressDialog progressDialog=new ProgressDialog(AccountVerificationActivity.this);
                progressDialog.setMessage("Loading....");
                progressDialog.show();
                progressDialog.setProgressStyle(R.style.DialogTheme);
                String otp = pinView.getText().toString();
                apiInterface = ApiClient.getClient().create(ApiInterface.class);
                final Call<VerifyOtpResponse> verifyOTPModelCall = apiInterface.verifyOTPModel(otp,mobile,"111");
                verifyOTPModelCall.enqueue(new Callback<VerifyOtpResponse>() {
                    @Override
                    public void onResponse(Call<VerifyOtpResponse> call, Response<VerifyOtpResponse> response) {
                        if (response.isSuccessful());
                        VerifyOtpResponse verifyOtpResponse = response.body();
                        if (verifyOtpResponse.status.equals("10100")){
                            progressDialog.dismiss();
                            VerifyOtpDataResponse verifyOtpDataResponse=verifyOtpResponse.data;
                            jwt=verifyOtpDataResponse.jwt;
                            user_id=verifyOtpDataResponse.userId;
                            user_name=verifyOtpDataResponse.userName;
                            email=verifyOtpDataResponse.email;
                            mobile=verifyOtpDataResponse.mobile;
                            userSessionManager.createUserLoginSession(jwt, user_id, user_name, email, mobile);
                            Toast.makeText(AccountVerificationActivity.this,verifyOtpResponse.message , Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(AccountVerificationActivity.this, MainActivity.class);
                            startActivity(intent);
                        }
                        else if (verifyOtpResponse.status.equals("10200")){
                            progressDialog.dismiss();
                            Toast.makeText(AccountVerificationActivity.this, verifyOtpResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<VerifyOtpResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(AccountVerificationActivity.this,t.getMessage() , Toast.LENGTH_SHORT).show();
                    }
                });
            }else {
                Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void getOTP() {
        if (checkInternet) {
            final ProgressDialog progressDialog=new ProgressDialog(AccountVerificationActivity.this);
            progressDialog.setMessage("Loading....");
            progressDialog.show();
            progressDialog.setProgressStyle(R.style.DialogTheme);
            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ResendOtpResponse> call=apiInterface.getOTPModel(mobile);
            call.enqueue(new Callback<ResendOtpResponse>() {
                @Override
                public void onResponse(Call<ResendOtpResponse> call, Response<ResendOtpResponse> response) {
                    if (response.isSuccessful());
                    ResendOtpResponse resendOtpResponse=response.body();
                    if (resendOtpResponse.status.equals("10100")){
                        progressDialog.dismiss();
                        Toast.makeText(AccountVerificationActivity.this, resendOtpResponse.message, Toast.LENGTH_SHORT).show();
                    }
                    else if (resendOtpResponse.status.equals("10200")){
                        progressDialog.dismiss();
                        Toast.makeText(AccountVerificationActivity.this, resendOtpResponse.message, Toast.LENGTH_SHORT).show();
                    }
                    else if (resendOtpResponse.status.equals("10300")){
                        progressDialog.dismiss();
                        Toast.makeText(AccountVerificationActivity.this, resendOtpResponse.message, Toast.LENGTH_SHORT).show();
                    }
                    else if (resendOtpResponse.status.equals("10400")){
                        progressDialog.dismiss();
                        Toast.makeText(AccountVerificationActivity.this, resendOtpResponse.message, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResendOtpResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(AccountVerificationActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        }else {
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
}
