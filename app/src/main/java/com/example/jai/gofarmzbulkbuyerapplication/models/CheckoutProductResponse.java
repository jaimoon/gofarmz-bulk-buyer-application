package com.example.jai.gofarmzbulkbuyerapplication.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class CheckoutProductResponse {


    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("product_name")
    @Expose
    public String productName;
    @SerializedName("total_price")
    @Expose
    public Integer totalPrice;
    @SerializedName("grand_total")
    @Expose
    public Integer grandTotal;
    @SerializedName("product_id")
    @Expose
    public String productId;
    @SerializedName("images")
    @Expose
    public String images;
    @SerializedName("mrp_price")
    @Expose
    public String mrpPrice;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("purchase_quantity")
    @Expose
    public String purchaseQuantity;
    @SerializedName("unit_name")
    @Expose
    public String unitName;
    @SerializedName("ESSENTIAL")
    @Expose
    public List<CheckoutProductDetailsResponse> eSSENTIAL = null;
    @SerializedName("discount")
    @Expose
    public Integer discount;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("productName", productName).append("totalPrice", totalPrice).append("grandTotal", grandTotal).append("productId", productId).append("images", images).append("mrpPrice", mrpPrice).append("type", type).append("purchaseQuantity", purchaseQuantity).append("unitName", unitName).append("eSSENTIAL", eSSENTIAL).append("discount", discount).toString();
    }

}
