package com.example.jai.gofarmzbulkbuyerapplication.utils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    /* public static String BASE_URL = "http://gofarmzv2.learningslot.in/";
    public static String IMAGE_URL = "http://gofarmzv2.learningslot.in//images/products/";
    public static String BASEIMAGEURL_70x70 = "http://gofarmzv2.learningslot.in//images/products/70x70/";
    public static String BASEIMAGEURL_400x400 = "http://gofarmzv2.learningslot.in//images/products/400x400/";
    public static String BASEIMAGEURL_280x280 = "http://gofarmzv2.learningslot.in//images/products/280x280/";
*/




        public static final String BASE_URL = "http://www.learningslot.com/learningslot.in/bulk/";
        //public static final String BASE_URL = "http://gofarmzv2.learningslot.in/";
        public static final String IMAGE_URL = "http://gofarmzv2.learningslot.in/images/products/";
        private static Retrofit retrofit = null;
        public static Retrofit getClient() {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .build();
            return new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
    }

}
