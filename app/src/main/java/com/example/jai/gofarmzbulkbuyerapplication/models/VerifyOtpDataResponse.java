package com.example.jai.gofarmzbulkbuyerapplication.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class VerifyOtpDataResponse {

    @SerializedName("jwt")
    @Expose
    public String jwt;
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("user_name")
    @Expose
    public String userName;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("mobile")
    @Expose
    public String mobile;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("jwt", jwt).append("userId", userId).append("userName", userName).append("email", email).append("mobile", mobile).toString();
    }
}
