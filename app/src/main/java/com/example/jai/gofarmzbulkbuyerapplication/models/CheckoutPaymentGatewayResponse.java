package com.example.jai.gofarmzbulkbuyerapplication.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class CheckoutPaymentGatewayResponse {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("logo")
    @Expose
    public String logo;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("logo", logo).toString();
    }


}
