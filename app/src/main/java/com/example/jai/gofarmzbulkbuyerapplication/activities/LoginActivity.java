package com.example.jai.gofarmzbulkbuyerapplication.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jai.gofarmzbulkbuyerapplication.MainActivity;
import com.example.jai.gofarmzbulkbuyerapplication.R;
import com.example.jai.gofarmzbulkbuyerapplication.models.LoginDataResponse;
import com.example.jai.gofarmzbulkbuyerapplication.models.LoginModel;
import com.example.jai.gofarmzbulkbuyerapplication.models.LoginResponse;
import com.example.jai.gofarmzbulkbuyerapplication.utils.ApiClient;
import com.example.jai.gofarmzbulkbuyerapplication.utils.ApiInterface;
import com.example.jai.gofarmzbulkbuyerapplication.utils.NetworkChecking;
import com.example.jai.gofarmzbulkbuyerapplication.utils.UserSessionManager;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    UserSessionManager userSessionManager;
    String deviceId,jwt,user_id,user_name,user_email,user_mobile;
    ApiInterface apiInterface;
    @BindView(R.id.username_edt)
    EditText usernameEdt;
    @BindView(R.id.password_edt)
    EditText passwordEdt;
    @BindView(R.id.forgotPassword_txt)
    TextView forgotPasswordTxt;
    @BindView(R.id.login_btn)
    Button loginBtn;
    @BindView(R.id.or)
    TextView or;
    @BindView(R.id.reg_btn)
    Button regBtn;
    @BindView(R.id.log_rl)
    RelativeLayout logRl;
    private boolean checkInternet;
    ProgressDialog pprogressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

//        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        checkInternet = NetworkChecking.isConnected(this);

        deviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        userSessionManager = new UserSessionManager(this);
        userSessionManager.createDeviceId(deviceId);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);

    }

    @OnClick({R.id.forgotPassword_txt, R.id.login_btn, R.id.reg_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.forgotPassword_txt:
                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);

                startActivity(intent);
                break;
            case R.id.login_btn:
                login();
                break;
            case R.id.reg_btn:
                reg();
                break;
        }
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    private void login() {

        if (checkInternet) {
            String email = usernameEdt.getText().toString();
            String password = passwordEdt.getText().toString();

            if(!isEmailValid(email) || email.length()==0) {
                Toast.makeText(LoginActivity.this,"Enter Valid Email",Toast.LENGTH_SHORT).show();
            }else if(password.length()==0){
                Toast.makeText(LoginActivity.this,"Enter Valid Password",Toast.LENGTH_SHORT).show();
            }else {
                pprogressDialog = new ProgressDialog(LoginActivity.this);
                pprogressDialog.setMessage("Please wait......");
                pprogressDialog.setProgressStyle(R.style.DialogTheme);
                pprogressDialog.show();

                apiInterface = ApiClient.getClient().create(ApiInterface.class);

                Call<LoginResponse> call=apiInterface.loginModel(email,password,"111");
                call.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        if (response.isSuccessful());
                        LoginResponse loginResponse=response.body();
                        if (loginResponse.status.equals("10100")){
                            pprogressDialog.dismiss();
                            Toast.makeText(LoginActivity.this, loginResponse.message, Toast.LENGTH_SHORT).show();
                            LoginDataResponse loginDataResponse=loginResponse.data;

                            jwt = loginDataResponse.jwt;
                            user_id =loginDataResponse.userId;
                            user_name =loginDataResponse.userName;
                            user_email = loginDataResponse.email;
                            user_mobile = loginDataResponse.mobile;
                            userSessionManager.createUserLoginSession(jwt, user_id, user_name, user_email, user_mobile);
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                        }
                        else if (loginResponse.status.equals("10200")){
                            pprogressDialog.dismiss();
                            Toast.makeText(LoginActivity.this, loginResponse.message, Toast.LENGTH_SHORT).show();

                        }
                        else if (loginResponse.status.equals("10300")){
                            pprogressDialog.dismiss();
                            Toast.makeText(LoginActivity.this, loginResponse.message, Toast.LENGTH_SHORT).show();
                        }
                        else if (loginResponse.status.equals("10400")){
                            pprogressDialog.dismiss();
                            Toast.makeText(LoginActivity.this, loginResponse.message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        pprogressDialog.dismiss();
                        Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.e("failed",t.getMessage());

                    }
                });
            }

        }else {
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void reg() {
        if (checkInternet) {
            Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
            startActivity(intent);
        }else {
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }
}
