package com.example.jai.gofarmzbulkbuyerapplication.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jai.gofarmzbulkbuyerapplication.R;
import com.example.jai.gofarmzbulkbuyerapplication.models.LogoutResponse;
import com.example.jai.gofarmzbulkbuyerapplication.models.ProfileAddressResponse;
import com.example.jai.gofarmzbulkbuyerapplication.models.ProfileDataResponse;
import com.example.jai.gofarmzbulkbuyerapplication.models.ProfileResponse;
import com.example.jai.gofarmzbulkbuyerapplication.utils.ApiClient;
import com.example.jai.gofarmzbulkbuyerapplication.utils.ApiInterface;
import com.example.jai.gofarmzbulkbuyerapplication.utils.NetworkChecking;
import com.example.jai.gofarmzbulkbuyerapplication.utils.UserSessionManager;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    EditText picode_edt, email_edt;
    RadioGroup rg;
    RadioButton male_rb, female_rb;
    ImageView close_img;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    String deviceId, token, user_id, mobile_verufy_status, mobile, email, gender, name, gender_value;
    Typeface light, regular, bold;
    TextView address_text, user_name, mobile_no, gender_title, logout_text, change_password_text, edit_profile, verify_mob_text;
    Button save_btn;
    ImageView verf_mob_iv;
    RelativeLayout location_layout;
    String send_gender;
    ApiInterface apiInterface;
    ProgressDialog pprogressDialog;
    ProfileAddressResponse profileAddressResponse;
    ProfileDataResponse profileDataResponse;
    Button change_address;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_activity);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();

        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);

        user_name = findViewById(R.id.user_name);
//        user_name.setTypeface(bold);
        mobile_no = findViewById(R.id.mobile_no);
//        mobile_no.setTypeface(regular);
        verify_mob_text = findViewById(R.id.verify_mob_text);
        email_edt = findViewById(R.id.email_edt);
        address_text = findViewById(R.id.address_text);
//        address_text.setTypeface(bold);
        location_layout = findViewById(R.id.location_layout);
        verf_mob_iv = findViewById(R.id.verf_mob_iv);

        male_rb = findViewById(R.id.male_rb);
//        male_rb.setTypeface(light);
        female_rb = findViewById(R.id.female_rb);
//        female_rb.setTypeface(light);
        edit_profile = findViewById(R.id.edit_profile);
//        edit_profile.setTypeface(bold);
        edit_profile.setOnClickListener(this);

        logout_text = findViewById(R.id.logout_text);
        logout_text.setOnClickListener(this);
        change_password_text = findViewById(R.id.change_password_text);
        change_password_text.setOnClickListener(this);
        change_address = findViewById(R.id.change_address);
        change_address.setOnClickListener(this);
        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);

        getProfileData();

    }

    private void getProfileData() {

        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {

            pprogressDialog = new ProgressDialog(ProfileActivity.this);
            pprogressDialog.setMessage("Please wait......");
            pprogressDialog.setProgressStyle(R.style.DialogTheme);
            pprogressDialog.show();
            apiInterface = ApiClient.getClient().create(ApiInterface.class);
            Call<ProfileResponse> call = apiInterface.Profile(user_id);
            call.enqueue(new Callback<ProfileResponse>() {
                @Override
                public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                    if (response.isSuccessful()) ;
                    ProfileResponse profileResponse = response.body();
                    if (profileResponse.status.equals("10100")) {
                        pprogressDialog.dismiss();
                        ProfileDataResponse profileDataResponse = profileResponse.data;
                        user_name.setText(profileDataResponse.name);
                        mobile_no.setText(profileDataResponse.mobile);
                        email_edt.setText(profileDataResponse.email);
                        gender=profileDataResponse.gender;
                        if(gender!=null){
                            send_gender=gender;
                            if (gender.equalsIgnoreCase("Male")) {
                                male_rb.setChecked(true);
                            } else if (gender.equalsIgnoreCase("Female")) {
                                female_rb.setChecked(true);
                            }
                        }



//                        if (profileDataResponse.mobileVerifyStatus.equals("1")){
//                            verf_mob_iv.setVisibility(View.VISIBLE);
//                        }

                        Toast.makeText(ProfileActivity.this, profileResponse.message, Toast.LENGTH_SHORT).show();

                    } else if (profileResponse.status.equals("10300")) {
                        pprogressDialog.dismiss();
                        Toast.makeText(ProfileActivity.this, profileResponse.message, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ProfileResponse> call, Throwable t) {
                    pprogressDialog.dismiss();
                    Toast.makeText(ProfileActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        if (v == edit_profile || v == mobile_no) {
            Intent updateIntent = new Intent(ProfileActivity.this, EditProfileActivity.class);
//            updateIntent.putExtra("name",profileAddressResponse.name);
//            updateIntent.putExtra("mobile",profileAddressResponse.contactNo);
//            updateIntent.putExtra("email",profileDataResponse.email);
//            if(gender_value!=null && gender_value.length()>0)
//            updateIntent.putExtra("gender",gender_value);
            startActivity(updateIntent);
        }
        if (v == change_password_text) {
            Intent intent = new Intent(ProfileActivity.this, ChangePasswordActivity.class);
            startActivity(intent);
        }
        if (v == close_img) {

            finish();
        }
        if (v == logout_text) {
            userSessionManager.logoutUser();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
        if (v == change_address) {

            startActivity(new Intent(ProfileActivity.this, AddAddressActivity.class));
        }


    }

    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == R.id.male_rb) {
            male_rb.setChecked(true);
            gender_value = "Male";
        } else {
            female_rb.setChecked(true);
            gender_value = "Female";
        }

    }
}
