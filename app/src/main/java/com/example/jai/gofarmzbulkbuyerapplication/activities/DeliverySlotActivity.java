package com.example.jai.gofarmzbulkbuyerapplication.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jai.gofarmzbulkbuyerapplication.R;

public class DeliverySlotActivity extends AppCompatActivity implements View.OnClickListener {
    Button proceed_toPay_btn;
    ImageView close_img;
    TextView manage_address_txt;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.deliveryslot_activity);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        close_img=findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        proceed_toPay_btn=findViewById(R.id.proceed_toPay_btn);
        proceed_toPay_btn.setOnClickListener(this);

        manage_address_txt=findViewById(R.id.manage_address_txt);
        manage_address_txt.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == close_img){
            finish();
        }
        if (v == proceed_toPay_btn){
            startActivity(new Intent(DeliverySlotActivity.this,PaymentDetailsActivity.class));
        }

        if (v == manage_address_txt){
            startActivity(new Intent(DeliverySlotActivity.this,ChooseAddressActivity.class));
        }

    }
}
