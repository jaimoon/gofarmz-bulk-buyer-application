package com.example.jai.gofarmzbulkbuyerapplication.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.example.jai.gofarmzbulkbuyerapplication.MainActivity;
import com.example.jai.gofarmzbulkbuyerapplication.R;
import com.example.jai.gofarmzbulkbuyerapplication.utils.UserSessionManager;

public class SplashScreenActivity extends AppCompatActivity {
    int SPLASH_TIME_OUT = 1850;
    RelativeLayout splash_screen;
    UserSessionManager session;
    String device_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        session = new UserSessionManager(getApplicationContext());

        splash_screen = findViewById(R.id.activity_splash_screen);

        device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        session.createDeviceId(device_id);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    if (session.checkLogin() != false) {
                        Intent i = new Intent(SplashScreenActivity.this, LoginActivity.class);
                        startActivity(i);
                        finish();
                    } else {
                        Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                }
            }, SPLASH_TIME_OUT);
        } else {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {

                    if (session.checkLogin() != false) {
                        Intent i = new Intent(SplashScreenActivity.this, LoginActivity.class);
                        startActivity(i);
                        finish();
                    } else {
                        Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                }
            }, SPLASH_TIME_OUT);
        }
    }
}
