package com.example.jai.gofarmzbulkbuyerapplication.models;

public class PaymentProductModel {
    String p_name;
    String p_qtn;
    String p_price;

    public PaymentProductModel(String p_name, String p_qtn, String p_price) {
        this.p_name = p_name;
        this.p_qtn = p_qtn;
        this.p_price = p_price;
    }

    public String getP_name() {
        return p_name;
    }

    public void setP_name(String p_name) {
        this.p_name = p_name;
    }

    public String getP_qtn() {
        return p_qtn;
    }

    public void setP_qtn(String p_qtn) {
        this.p_qtn = p_qtn;
    }

    public String getP_price() {
        return p_price;
    }

    public void setP_price(String p_price) {
        this.p_price = p_price;
    }
}
