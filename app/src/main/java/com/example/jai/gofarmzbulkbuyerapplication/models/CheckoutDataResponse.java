package com.example.jai.gofarmzbulkbuyerapplication.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class CheckoutDataResponse {


    @SerializedName("mobile_verify_status")
    @Expose
    public String mobileVerifyStatus;
    @SerializedName("mobile")
    @Expose
    public String mobile;
    @SerializedName("address")
    @Expose
    public List<CheckoutAddressResponse> address = null;
    @SerializedName("enable_order_closing")
    @Expose
    public Integer enableOrderClosing;
    @SerializedName("order_closing_txt")
    @Expose
    public String orderClosingTxt;
    @SerializedName("products")
    @Expose
    public List<CheckoutProductResponse> products = null;
    @SerializedName("finalTotal")
    @Expose
    public Integer finalTotal;
    @SerializedName("finalDiscount")
    @Expose
    public Integer finalDiscount;
    @SerializedName("payment_gateway")
    @Expose
    public List<CheckoutPaymentGatewayResponse> paymentGateway = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("mobileVerifyStatus", mobileVerifyStatus).append("mobile", mobile).append("address", address).append("enableOrderClosing", enableOrderClosing).append("orderClosingTxt", orderClosingTxt).append("products", products).append("finalTotal", finalTotal).append("finalDiscount", finalDiscount).append("paymentGateway", paymentGateway).toString();
    }
}
