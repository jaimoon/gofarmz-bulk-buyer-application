package com.example.jai.gofarmzbulkbuyerapplication.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.example.jai.gofarmzbulkbuyerapplication.R;
import com.example.jai.gofarmzbulkbuyerapplication.adapters.CategoryProductAdapter;
import com.example.jai.gofarmzbulkbuyerapplication.models.CategoryDetailsModel;
import com.example.jai.gofarmzbulkbuyerapplication.utils.ScrollableGridView;

import java.util.ArrayList;

public class CategoryDetailsActivity extends AppCompatActivity implements View.OnClickListener{
    ImageView close_img;
    ScrollableGridView box_products_grid;
    ArrayList<CategoryDetailsModel> boxProductList = new ArrayList<>();
    CategoryProductAdapter categoryProductAdapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.categorydetails_activity);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        close_img=findViewById(R.id.close_img);
        close_img.setOnClickListener(this);

        box_products_grid=findViewById(R.id.box_products_grid);

        boxProductList.add(new CategoryDetailsModel(R.drawable.cart,"tamoto","1kg"));
        boxProductList.add(new CategoryDetailsModel(R.drawable.cart,"tamoto","1kg"));
        boxProductList.add(new CategoryDetailsModel(R.drawable.cart,"tamoto","1kg"));
        boxProductList.add(new CategoryDetailsModel(R.drawable.cart,"tamoto","1kg"));
        boxProductList.add(new CategoryDetailsModel(R.drawable.cart,"tamoto","1kg"));
        boxProductList.add(new CategoryDetailsModel(R.drawable.cart,"tamoto","1kg"));
        boxProductList.add(new CategoryDetailsModel(R.drawable.cart,"tamoto","1kg"));
        boxProductList.add(new CategoryDetailsModel(R.drawable.cart,"tamoto","1kg"));
        boxProductList.add(new CategoryDetailsModel(R.drawable.cart,"tamoto","1kg"));
        boxProductList.add(new CategoryDetailsModel(R.drawable.cart,"tamoto","1kg"));
        boxProductList.add(new CategoryDetailsModel(R.drawable.cart,"tamoto","1kg"));
        boxProductList.add(new CategoryDetailsModel(R.drawable.cart,"tamoto","1kg"));
        boxProductList.add(new CategoryDetailsModel(R.drawable.cart,"tamoto","1kg"));
        boxProductList.add(new CategoryDetailsModel(R.drawable.cart,"tamoto","1kg"));


        categoryProductAdapter=new CategoryProductAdapter(CategoryDetailsActivity.this,boxProductList);
        box_products_grid.setAdapter(categoryProductAdapter);
        box_products_grid.setExpanded(true);

    }

    @Override
    public void onClick(View v) {
        if (v == close_img){
            finish();
        }

    }
}
