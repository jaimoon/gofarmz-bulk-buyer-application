package com.example.jai.gofarmzbulkbuyerapplication.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jai.gofarmzbulkbuyerapplication.R;
import com.example.jai.gofarmzbulkbuyerapplication.activities.CartActivity;
import com.example.jai.gofarmzbulkbuyerapplication.models.CartModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CartRecyclerAdapter extends RecyclerView.Adapter<CartRecyclerAdapter.Holder> {

    ArrayList<CartModel> arrayList;
    Context context;

    public CartRecyclerAdapter(ArrayList<CartModel> arrayList, Context cartActivity) {
        this.arrayList=arrayList;
        this.context=cartActivity;
    }

    @NonNull
    @Override
    public CartRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_cart, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CartRecyclerAdapter.Holder holder, int i) {
        holder.product_name.setText(arrayList.get(i).getP_name());
        holder.price_txt.setText(arrayList.get(i).getP_price());
        Picasso.with(context).load(arrayList.get(i).getImage()).into(holder.product_img);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class Holder extends RecyclerView.ViewHolder{
        ImageView product_img,delete_img;
        TextView product_name,view_txt,price_txt;

        public Holder(@NonNull View itemView) {
            super(itemView);

            product_img=itemView.findViewById(R.id.product_img);
            delete_img=itemView.findViewById(R.id.delete_img);
            product_name=itemView.findViewById(R.id.product_name);
            view_txt=itemView.findViewById(R.id.view_txt);
            price_txt=itemView.findViewById(R.id.price_txt);
        }
    }
}
