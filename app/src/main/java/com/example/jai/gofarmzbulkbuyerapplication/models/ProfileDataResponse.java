package com.example.jai.gofarmzbulkbuyerapplication.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class ProfileDataResponse {


    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("gender")
    @Expose
    public String gender;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("mobile")
    @Expose
    public String mobile;
    @SerializedName("mobile_verify_status")
    @Expose
    public String mobileVerifyStatus;
    @SerializedName("email_verify_status")
    @Expose
    public String emailVerifyStatus;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("gender", gender).append("email", email).append("mobile", mobile).append("mobileVerifyStatus", mobileVerifyStatus).append("emailVerifyStatus", emailVerifyStatus).toString();
    }

}
