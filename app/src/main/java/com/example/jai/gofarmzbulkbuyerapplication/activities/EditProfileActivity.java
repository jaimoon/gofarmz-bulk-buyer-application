package com.example.jai.gofarmzbulkbuyerapplication.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.jai.gofarmzbulkbuyerapplication.MainActivity;
import com.example.jai.gofarmzbulkbuyerapplication.R;
import com.example.jai.gofarmzbulkbuyerapplication.models.EditProfileResponse;
import com.example.jai.gofarmzbulkbuyerapplication.utils.ApiClient;
import com.example.jai.gofarmzbulkbuyerapplication.utils.ApiInterface;
import com.example.jai.gofarmzbulkbuyerapplication.utils.NetworkChecking;
import com.example.jai.gofarmzbulkbuyerapplication.utils.UserSessionManager;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    EditText username_edt,mobile_edt,email_edt;
    Button update_btn;
    ImageView close_img;
    RadioGroup rg;
    RadioButton male_rb, female_rb;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    String  deviceId, token, user_id,email,mobile,gender,name,send_gender;
    Typeface light,regular,bold;
    ApiInterface apiInterface;
    ProgressDialog pprogressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editprofile_activity);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        email_edt = findViewById(R.id.email_edt);
//        email_edt.setTypeface(light);
        mobile_edt = findViewById(R.id.mobile_edt);
//        mobile_edt.setTypeface(light);
        username_edt = findViewById(R.id.username_edt);
//        username_edt.setTypeface(light);

        update_btn=findViewById(R.id.update_btn);
        update_btn.setOnClickListener(this);
//        update_btn.setTypeface(bold);

        rg = findViewById(R.id.rg);
        rg.setOnCheckedChangeListener(this);
        male_rb = findViewById(R.id.male_rb);
//        male_rb.setTypeface(light);
        female_rb = findViewById(R.id.female_rb);
//        female_rb.setTypeface(light);

        close_img=findViewById(R.id.close_img);
        close_img.setOnClickListener(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        mobile=userDetails.get(UserSessionManager.USER_MOBILE);
        email=userDetails.get(UserSessionManager.USER_EMAIL);
        name=userDetails.get(UserSessionManager.USER_NAME);

        mobile_edt.setText(mobile);
        email_edt.setText(email);
        username_edt.setText(name);
        if(gender!=null){
            send_gender=gender;
            if (gender.equalsIgnoreCase("Male")) {
                male_rb.setChecked(true);
            } else if (gender.equalsIgnoreCase("Female")) {
                female_rb.setChecked(true);
            }
        }



    }

    @Override
    public void onClick(View v) {
        if (v == close_img) {

            finish();
        }
        if (v == update_btn) {
            String moblie=mobile_edt.getText().toString();
            if(moblie==null || moblie.length()==0)
            {
                Toast.makeText(this,"Enter Mobile No",Toast.LENGTH_SHORT).show();
            }
            else if(send_gender==null || send_gender.length()==0)
            {
                Toast.makeText(this,"Select Gender",Toast.LENGTH_SHORT).show();
            }else{
                updateProfile();
            }

        }


    }

    private void updateProfile() {
        pprogressDialog = new ProgressDialog(EditProfileActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        pprogressDialog.show();
        checkInternet = NetworkChecking.isConnected(this);
        if(checkInternet)
        {
            apiInterface= ApiClient.getClient().create(ApiInterface.class);
            Call<EditProfileResponse> call=apiInterface.EditProfile(user_id,username_edt.getText().toString(),email_edt.getText().toString(),mobile_edt.getText().toString(),send_gender);
            call.enqueue(new Callback<EditProfileResponse>() {
                @Override
                public void onResponse(Call<EditProfileResponse> call, Response<EditProfileResponse> response) {
                    if (response.isSuccessful());
                    EditProfileResponse editProfileResponse=response.body();
                    if (editProfileResponse.status.equals("10100")){
                        pprogressDialog.dismiss();
                        Toast.makeText(EditProfileActivity.this, editProfileResponse.message, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(EditProfileActivity.this,MainActivity.class));
                    }
                    else if (editProfileResponse.status.equals("10300")){
                        pprogressDialog.dismiss();
                        Toast.makeText(EditProfileActivity.this, editProfileResponse.message, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<EditProfileResponse> call, Throwable t) {
                    pprogressDialog.dismiss();
                    Toast.makeText(EditProfileActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
        else
        {
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if(checkedId==R.id.male_rb){
            male_rb.setChecked(true);
            send_gender="Male";
        }else {
            female_rb.setChecked(true);
            send_gender="Female";
        }

    }
}
